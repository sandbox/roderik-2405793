<?php

// Note: feeds by Wyz have different way of preparing node teaser/body
// for output than feeds by Qrios. Qrios feeds have not been changed/checked
// for security YET. Stuff should be unified sometime.

// Note: various site specific assumptions made for various fields.

/**
 * Returns array of entities (likely commerce products; for some sites, nodes)
 * whose data should be output.
 *
 * @param string $query_type
 *   An identifier for the feed, which determines filtering in some way.


 * Featuredtype only matters if $tid_featured is non-empty:
 * 'extra' means an extra field will be returned, containing the same nid value
 *         IF the node has $tid_featured as well.
 * 'limit' means the nid is only returned if the node _also_ as $tid_featured.
 *
 * TODO My (RM) above description of an earlier module version is still in there,
 * but does not match the code below anymore. Fix either to match the other.
 *
 * This function should be called several times:
 * - the first time, data gets collected, the $tid argument is used,
 *   Max. 100 products are returned (or FALSE for error)
 * - the 2nd - (x-1)th time, more products (max. 100) are returned for the same
 *   tid. $tid argument is ignored.
 * - the last time, an empty array is returned to signify there's no more input.
 *   Internal state is reset so that the next call will have the 'first time' behavior.
 */
function _q_prodfeeds_get_products($query_type, $filter_key, $tid_featured = '', $featuredtype = 'limit') {

  static $products_to_process = FALSE;

  $filterfield = variable_get('q_prodfeeds_apifield', '');
  if (!$filterfield || $products_to_process === array()) {
    // Last time in loop; $products_to_process is always empty.
    $products_to_process = FALSE;
    return array();
  }
  $field_info = field_info_field($filterfield);
  $filter_value = variable_get('q_prodfeeds_filter_' . $filter_key, '');
  if (!$filter_value) {
    // $filter_key can also be among the allowed values of the field directly.
    // TODO make better comment.
    if (isset($field_info['settings']['allowed_values'][$filter_key])) {
      $filter_value = $filter_key;
    }
  }
  if (!$filter_value) {
    return array();
  }

  $entity_bundle = variable_get('q_prodfeeds_entity_bundle', 'commerce_product:');
  $eb = explode(':', $entity_bundle);
  if (!$products_to_process) {

    // First time in loop


    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', $eb[0]);
    if (!empty($eb[1])) {
      $query->entityCondition('bundle', $eb[1]);
    }
    $query->propertyCondition('status', 1);
    if (isset($field_info['type'])
        && $field_info['type'] == 'taxonomy_term_reference') {
      $query->fieldCondition($filterfield, 'tid', $filter_value, '=');
    }
    else {
      $query->fieldCondition($filterfield, 'value', $filter_value, '=');
    }
    $entities = $query->execute();

    if (isset($entities['commerce_product'])) {
      $products_to_process = array_keys($entities['commerce_product']);

      $limit = variable_get('q_prodfeeds_limit_' . $query_type, '');
      if ($limit && is_numeric($limit)
          && count($products_to_process) > $limit) {
        $products_to_process = array_slice($products_to_process, 0, $limit);
      }
//      // Optional, guarantees consistency with older implementation
//      sort($products_to_process);


      // Additional one-time preprocessing:

      // Get all relevant terms whose data we need cached in one go -
      // this is better than lots of small DB queries every time we need a term.
      // TODO un-hardcode?
      taxonomy_get_tree(1, 0, NULL, TRUE);
      taxonomy_get_tree(2, 0, NULL, TRUE);
    }
  }

  // Get maximum 100 items to return for this 'iteration'
  $process_now = array_splice($products_to_process, 0, min(count($products_to_process), 100));

  $entities = entity_load($eb[0], $process_now);

  if ($entities && $eb[0] == 'commerce_product') {
    // Products themselves have no paths. Get the nids for the display nodes,
    // because
    // - path can be derived from nid
    // - nid, unlike path, can't be bulk-queried so we'll derive that later.

    // Getting product->nid mappings for a number of products at once is not
    // possible using EntityFieldQuery. The optimization warrants direct SQL.
    $p_ref_field = 'field_product';
    $query = db_select('field_data_' . $p_ref_field, 'nf');
    $query->condition('nf.entity_type', 'node');
//    if ($dest_bundle) {
//      $query->condition('bundle', $dest_bundle);
//    }
    $query->condition('nf.' . $p_ref_field . '_product_id', $process_now, 'IN');
    $query->addField('nf', $p_ref_field . '_product_id', 'pid');
    $query->addField('nf', 'entity_id', 'nid');
    $result = $query->execute();

    // Put 'nid' in the entity. Iterate over each found display node, i.e.
    // - Products may theoretically get no nid assigned
    // - Assume only one product display per product (More values just overwrite.)
    foreach ($result as $row) {
      $entities[$row->pid]->nid = $row->nid;
    }
  }

  return $entities;

  // Old code (not clean, partially edited)

  // $sql = 'SELECT DISTINCT n.nid FROM {node} n INNER JOIN {taxonomy_index} tn ON n.nid = tn.nid';
  //
  // // $where = array('tn.tid = :tid AND n.status = 1 AND n.moderate = 0');
  // $where = array('tn.tid = :tid AND n.status = 1');
  //
  // $args = array(':tid' => $tid);
  // if (!empty($tid_featured)) {
  //   switch ($featuredtype) {
  //     case 'extra':
  //       $sql = str_replace('n.nid FROM ', 'n.nid, s.nid AS featured FROM ', $sql)
  //         . " LEFT JOIN (SELECT nid FROM {term_node} WHERE tid = :tid) s on n.nid = s.nid";
  //       array_unshift($args, $tid_featured);
  //       break;
  //
  //     default:
  //       $sql .= ' INNER JOIN {term_node} tn2 ON n.nid = tn2.nid';
  //       $where[] = 'tn2.tid = :tid';
  //       $args[':tid'] = $tid_featured;
  //   }
  // }
  // if (empty($limit)) {
  //   echo $sql = $sql . ' WHERE ' . join(' AND ', $where) . ' ORDER BY n.created DESC';
  //   $result =  db_query($sql, $args);
  //   foreach($result as $record) {
  //     die(print($record));
  //   }
  // // }
  // // else {
  // //   return db_query_range($sql . ' WHERE ' . join(' AND ', $where) . ' ORDER BY n.created DESC'
  // //       , $args, 0, $limit);
  // // }
}

/**
 * Generate array from a product object containing all
 * relevant attributes ready to be output.
 * Converting to an array here makes it easier to do installation specific
 * overrides.
 *
 * @param object $entity
 * @param string $type
 * @return array
 */
function _q_prodfeeds_prepare_product($entity, $type, $field_formats = array(), $cdata_fields = array()) {

  $default_display_type = variable_get('q_prodfeeds_bodytype_' . $type, 'teaser');
  // Translate body/teaser to text_field_formatter type names
  switch ($default_display_type) {
    case 'body':
      $default_display_type = 'text_default';
      break;
    case 'teaser':
      $default_display_type = 'text_summary_or_trimmed';
      break;
  }

  // Load brand and category names
  $brand = '';
  if (count($entity->field_product_brand)) {
    if ($term = taxonomy_term_load($entity->field_product_brand['und'][0]['tid'])) {
      $brand = $term->name;
    }
  }

  $category = '';
  $category_path = array();
  if (count($entity->field_product_category)) {
    // Our assumption is however that there is ONE consistent string of
    // ancestors, so all terms will have the same ancestor paths. It'll just be
    // longer or shorter for each tid.
    // Find the longest path of ancestors.
    foreach ($entity->field_product_category['und'] as $item) {
      $parents = taxonomy_get_parents_all($item['tid']);
      if (count($parents) > count($category_path)) {
        $category_path = $parents;
      }
    }
    if ($category_path) {
      $term = end($category_path);
      $category = $term->name;
    }
  }

  // The product image URL
  if (count($entity->field_product_image)) {
    $image_url = file_create_url($entity->field_product_image['und'][0]['uri']);
  }
  else {
    $image_url = '';
  }

  // Construct free-text fields first.
  $product = array(
    'title'    => $entity->title,
    'sku'      => $entity->sku,
    'brand'    => $brand,
    'type'     => (count($entity->field_model_type) ? $entity->field_model_type['und'][0]['value'] : ''),
    'category' => $category,
    'ean'      => (count($entity->field_product_ean) ? $entity->field_product_ean['und'][0]['value'] : ''),
    // don't use? 'nid' => $entity->nid,
    // NB specified as: field_product_gw_price or sale or discount price if applicable
    'price' => count($entity->field_product_action_price)
        ? sprintf("%01.2f", round($entity->field_product_action_price['und'][0]['amount'] / 100, 2) )
        : (count($entity->commerce_price)
          ? sprintf("%01.2f", round($entity->commerce_price['und'][0]['amount'] / 100, 2) ) : ''),
    'shipping' => (count($entity->field_product_shipping_costs) ? sprintf("%01.2f",  $entity->field_product_shipping_costs['und'][0]['value']) : ''),
    'stock' => (count($entity->commerce_stock) ? $entity->commerce_stock['und'][0]['value'] : ''),
  );
  if ($product['stock']) {
    $product['delivery'] = 'Uit voorraad leverbaar';
  }
  else {
    $field_info = field_info_field('field_delivery_diff');
    $value = $entity->field_delivery_diff['und'][0]['value'];
    if (isset($field_info['settings']['allowed_values'][$value])) {
      $product['delivery'] = $field_info['settings']['allowed_values'][$value];
    }
    else {
      $product['delivery'] = '';
    }
  }

  // Type specific alterations:
  // (This is a great place for putting documentation...)
  switch ($type) {
    case 'google':
      // Google wants ' EUR' suffix for prices, and commas for decimal separators.
      if (empty($product['price'])) {
        $product['price'] = '0,00 EUR';
      }
      else {
        $product['price'] = str_replace('.', ',', $product['price']) . ' EUR';
      }
      if (empty($product['shipping'])) {
        $product['shipping'] = '0,00 EUR';
      }
      else {
        $product['shipping'] = str_replace('.', ',', $product['shipping']) . ' EUR';
      }

      // GBW specific:

      // Category field can contain a path with subcategs.

      // You already have the last in $category
      array_pop($category_path);
      while ($term = array_pop($category_path)) {
        $product['category'] .= ' > ' . $term->name;
      }
      break;

    case 'affiliate4you':

      // At least Affiliate4You (we don't know about others, Google probably not)
      // _requires_ all price fields to have two digits. Zero digits is not ok.

      if (empty($product['shipping'])) {
        $product['shipping'] = '0.00';
      }

      // Images:
      // Let op: Product images in de datafeed moeten de volledige url bevatten naar het plaatje bij jullie op de server. Dit plaatje mag niet groter zijn dan 200kb of 500x500.

      // GBW specific:

      // 'product_image_large' style is 648x478.
      // There's a 'product_large' style in the site which is 430x430, but that is not used so it would
      //  cause all the images to be generated just for A4You (which makes getting them in bulk slow too).
      // 'product_image_medium' is 210x210; we'll use that.

      $image_url = image_style_url('product_image_medium',
        $entity->field_product_image['und'][0]['uri']);

      $product['subcategory'] = '';
      array_pop($category_path);
      if ($term = array_pop($category_path)) {
        $product['subcategory'] = $term->name;
      }
      break;

    case 'kieskeurig':

      /* Process delivery values, as Kieskeurig have specific formats:
        - Op werkdagen voor [xx.xx] betaald, morgen in huis
        - 1 t/m 2 werkdagen
        - 3 t/m 4 werkdagen
        - t/m 5 werkdagen
        - Langer dan 5 werkdagen
        - onbekend
       */

      // Value can be 'Uit voorraad leverbaar' (as above) or 'Op voorraad' (as
      // configured with allowed values)
      if (strpos($product['delivery'], 'voorraad') !== FALSE) {
        $product['delivery'] = shipping_timer_static_string('kieskeurig_feed');
      }
      elseif (strpos($product['delivery'], 'voorraad') !== FALSE) {
        // not sure if replacement is necessary but this way it matches the examples
        $product['delivery'] = str_replace('-', ' t/m ', $product['delivery']);
      }
      else {
        $product['delivery'] = 'onbekend';
      }


      // Product description:
      // old geboortewinkel (views) had unshortened description here, no CDATA, full html.
      // gpspuntnl had teaser, with tags stripped. Qoony the same.
      // TODO maybe make $field_formats a menu/callback function parameter.
      $field_formats['field_product_desc'] = 'text_default';
      break;
  }

  //// Fields with summary
  //
  $fieldname = 'field_product_desc';
  $item = $entity->{$fieldname}['und'][0];
  // TODO generalize. (needs generalizing '->type')
  $instance = field_info_instance('commerce_product', $fieldname, $entity->type);
  $langcode = 'und';
  $trim_length = 750; // arbitrary. Google says "recommend between 500 and 1000, possible up to 10.000". Encod/document that somewhere.

  //$product['description_nukethis'] =  str_replace('&nbsp;', ' ', strip_tags($entity->field_product_desc['und'][0]['safe_value']));

  // TODO:
  //$bodytype = variable_get('q_prodfeeds_bodytype_' . $feed_type, 'teaser');


  // Borrowed & reshuffled from text_field_formatter_view().
  // Supported: text_default, text_trimmed, text_summary_or_trimmed, text_plain.
  // otherwise emulate text_default.
  // (no text_plain option, for now.)
  $display_type = isset($field_formats[$fieldname])
    ? $field_formats[$fieldname] : $default_display_type;
  if ($display_type == 'text_plain') {
    $output = strip_tags($item['value']);
  }
  elseif ($display_type == 'text_summary_or_trimmed' && !empty($item['summary'])) {
    $output = _text_sanitize($instance, $langcode, $item, 'summary');
  }
  else {
    $output = _text_sanitize($instance, $langcode, $item, 'value');
    if (strpos($display_type, 'trimmed') !== FALSE) {
      $output = text_summary($output, $instance['settings']['text_processing'] ? $item['format'] : NULL, $trim_length);
    }
  }

  // ...and then get rid of hardcoded spaces.
  $product['description'] = str_replace('&nbsp;', ' ', $output);

  // It's now plain text so we don't need to do:
  //$cdata_fields['description'] = FALSE;
   //   <-- TODO when you get time, see whether $field_formats input should influence the setting of this. See also description below,
   //       which shows an assumption about input format based on $cdata - while we have $field_formats now.


  // Process free-text fields based on how we'll output them.
  // If inside CDATA, we assume the field can contain HTML and should not be
  //   re-encoded. We only do XSS check - if it hasn't been _text_sanitize()d.
  // Otherwise we run them through check_plain (which HTMLencodes it)
  //   + extra XML-validity encoding test.
  foreach ($product as $fieldname => $value) {
    if (isset($cdata_fields[$fieldname])) {
      if ($cdata_fields[$fieldname]) {
        $product[$fieldname] = filter_xss($product[$fieldname]);
      }
    }
    else {
      $product[$fieldname] = preg_replace('/&#0*39;/', '&apos;',
                                          check_plain($product[$fieldname]));
    }
  }


  // Add non-free-text fields (which must not be encoded).

  $product += array(
    'url' => url('node/'. $entity->nid, array('absolute' => TRUE)),
    'image_url' => $image_url,
  );

  return $product;
}

////

/**
 * Outputs product feed for vergelijk.nl
 */
function q_prodfeeds_vergelijk() {
  $feed_type = str_replace('q_prodfeeds_', '', __FUNCTION__);
  $result = q_prodfeeds_standard_query($feed_type,
      variable_get('q_prodfeeds_limit_' . $feed_type, '')
  );
  // A feed is not supposed to generate errors. If the query generated errors,
  // just output empty XML doc.
  
  $xml_feed = '<?xml version="1.0" encoding="UTF-8"?>
  <products>';
  while ($result = _q_prodfeeds_get_products($feed_type)) {
    foreach ($result as $entity) {
      if (!empty($entity->nid)) {

        $product = _q_prodfeeds_prepare_product($entity, $feed_type);
    
        $xml_feed .= '
        <product>
        <id>' . $product['id'] . '</id>
        <productcode>' . $product['sku'] . '</productcode>
        <merk>' . $product['brand'] . '</merk>
        <type>' . $product['category'] . '</type>
        <toevoeging-type></toevoeging-type>
        <extra-productbeschrijving>' .  $product['description'] . '</extra-productbeschrijving>    <partnumber></partnumber>
        <ean-code>' . $product['ean'] . '</ean-code>
        <prijs>' . $product['price'] . '</prijs>
        <verzendkosten>' .  $product['shipping']  . '</verzendkosten>
        <afhaalkosten>0.00</afhaalkosten>
        <levertijd>' .  $product['delivery']  . '</levertijd>
        <deeplink>' .  $product['url']  . '</deeplink>
        <imagelink>' .  $product['image_url']  . '</imagelink>
        <voorraad>' .  $product['quantity']  . '</voorraad>
        </product>';
      }
    }
  }
  $xml_feed .= '</products>';

  header('Content-Type: text/xml');
  echo $xml_feed;
  exit;
}

/**
 * Outputs product feed for kieskeurig.nl
 * TODO delete, unused
 */
function q_prodfeeds_kieskeurig() {
  $feed_type = str_replace('q_prodfeeds_', '', __FUNCTION__);
  $result = q_prodfeeds_standard_query($feed_type,
      variable_get('q_prodfeeds_limit_' . $feed_type, '')
  );
  // A feed is not supposed to generate errors. If the query generated errors,
  // just output empty XML doc.

  $xml_feed = '<?xml version="1.0" encoding="ISO-8859-1" ?>
  <Products>';
  while ($result = _q_prodfeeds_get_products($feed_type)) {
    foreach ($result as $entity) {
      if (!empty($entity->nid)) {

        $product = _q_prodfeeds_prepare_product($entity, $feed_type);
    
        $xml_feed .= '
          <Product>
          <Category>' . $product['category'] . '</Category>
          <SubCategory></SubCategory>
          <Brand>' . $product['brand'] . '</Brand >
          <ProductName>' . $product['title'] . '</ProductName>
          <Deeplink>' . $product['url'] . '</Deeplink>
          <Price>' . $product['price'] . '</Price>
          <DeliveryPeriod>' . $product['delivery'] . '</DeliveryPeriod>
          <DeliveryCosts >' . $product['shipping'] . '</DeliveryCosts>
          <OfferId></OfferId>
          <ProductVendorPartNr></ProductVendorPartNr>
          <ProductEAN>' . $product['ean'] . '</ProductEAN>
          <ProductDescription>' . $product['description'] . '</ProductDescription>
          <DeeplinkPicture>' . $product['image_url'] . '</DeeplinkPicture>
          <StockStatus>' . (($product['quantity'] > 0) ? 1: 0) . '</StockStatus>
          <ProductsInStock>' . $product['quantity'] . '</ProductsInStock>
          <PromotionText></PromotionText>
          </Product>
        ';
      }
    }
  }
  $xml_feed .= '</Products>';

  header('Content-Type: text/xml');
  echo $xml_feed;
  exit;
}

/**
 * Outputs product feed for beslist.nl
 */
function q_prodfeeds_beslist() {
  $feed_type = str_replace('q_prodfeeds_', '', __FUNCTION__);
  $result = q_prodfeeds_standard_query($feed_type,
      variable_get('q_prodfeeds_limit_' . $feed_type, '')
  );
  // A feed is not supposed to generate errors. If the query generated errors,
  // just output empty XML doc.

  $xml_feed = '<?xml version="1.0" encoding="ISO-8859-1" ?>
  <Products>';
  while ($result = _q_prodfeeds_get_products($feed_type)) {
    foreach ($result as $entity) {
      if (!empty($entity->nid)) {

        $product = _q_prodfeeds_prepare_product($entity, $feed_type);
    
        $xml_feed .= '
          <Product>
          <Titel>' . $product['title'] . '</Titel>
          <EAN>' . $product['ean'] . '</EAN>
          <Merk>' . $product['brand'] . '</Merk>
          <SKU>' . $product['sku'] . '</SKU>
          <Beschrijving>' . $product['description'] . '</Beschrijving>
          <Prijs>' . $product['price'] . '</Prijs>
          <Levertijd>' . $product['delivery'] . '</Levertijd>
          <Deeplink>' . $product['url'] . '</Deeplink>
          <Imagelocatie>' . $product['image_url'] . '</Imagelocatie>
          <Categorie>' . $product['category'] . '</Categorie>
          <Portokosten>' . $product['shipping'] . '</Portokosten>
          <Winkelproductcode>' . $product['id'] . '</Winkelproductcode>
          </Product>
        ';
      }
    }
  }
  $xml_feed .= '</Products>';

  header('Content-Type: text/xml');
  echo $xml_feed;
  exit;
}


/**
 * Outputs product feed for TradeTracker
 */
function q_prodfeeds_tradetracker() {
  $feed_type = str_replace('q_prodfeeds_', '', __FUNCTION__);
  $bodytype = variable_get('q_prodfeeds_bodytype_' . $feed_type, 'teaser');

  // A feed is not supposed to generate errors. If the query generated errors,
  // just output empty XML doc.

  $xml_feed = '<?xml version="1.0" encoding="UTF-8" ?>';
  $xml_feed .= '<productFeed version="1.0" timestamp="'.date("Ymd:G:i:s").'">';

  while ($result = _q_prodfeeds_get_products($feed_type)) {
    foreach ($result as $entity) {
      if (!empty($entity->nid)) {

        $product = _q_prodfeeds_prepare_product($entity, $feed_type);

        $xml_feed .= '
          <product id="' . $product['id'] . '">
            <name>' . $product['title'] . '</name>
            <price>' . $product['price'] . '</price>
            <description>
              <![CDATA[' . $product['description'] . ']]>
            </description>
            <productURL>' . $product['url'] . '</productURL>
            <imageURL>' . $product['image_url'] . '</imageURL>
            <categories><category name="' . $product['category'] . '" /></categories>
            <additional>
              <field name="EAN" value="' . $product['ean'] . '" />
              <field name="SKU" value="' . $product['sku'] . '" />
            </additional>
          </product>
        ';
      }
    }
  }
  $xml_feed .= '</productFeed>';

  header('Content-Type: text/xml');
  echo $xml_feed;
  exit;
}


/**
 * Outputs product feed for Google Merchant (Wyz)
 */
function q_prodfeeds_google() {

  // TODO: un-hardcode this sometime!
  $localuri = 'google-merchant.xml';

  // TODO: do we still need feed_type here? For other feeds we encode it in function args?
  $feed_type = str_replace('q_prodfeeds_', '', __FUNCTION__);

  $lastupdate = mktime(0, 0, 0, 1, 1, 2000);
  $xml_feed = '';
  //TODO check extra function arguments
  while ($result = _q_prodfeeds_get_products($feed_type, $feed_type,
    variable_get('q_prodfeeds_tid_featured_' . $feed_type, ''),
    'extra')) {

    foreach ($result as $entity) {
      if (!empty($entity->nid)) {

        $product = _q_prodfeeds_prepare_product($entity, $feed_type, array(
          // This is what it was in GBW - do we want text_plain? TODO re-check specs.
          // This means the teaser/body selection from the settings screen is NOT heeded at the moment.
          'field_product_desc' => 'text_plain',
        ));


        // TODO :category can/should have 'Cat > Subcat'

        $xml_feed .= theme('q_prodfeeds_item_google', array('item' => $product));

        if ($entity->changed > $lastupdate) {
          $lastupdate = $entity->changed;
        }
      }
    }
  }

  // if ($xml_feed) { No; also output XML if feed contains no items.

  $thisdomain = variable_get('q_prodfeeds_myhost_google', '');
  if (empty($thisdomain)) {
    // Do not take SERVER_NAME since that is in the server config, i.e. the same
    // for all sites in a multisite install.
    $thisdomain = $_SERVER['HTTP_HOST'];
  }
  $xml_feed = theme('q_prodfeeds_feed_google',
    array(
      'feed' => array(
        'data'           => $xml_feed,
        'date_formatted' => date_format(date_create('@' . $lastupdate), 'Y-m-d\TH:i:s\Z'),
        'hostname'       => $thisdomain,
        'local_uri'      => $localuri,
        'sitename'       => ucfirst(str_replace('www.', '', $thisdomain)),
      )
    )
  );

  header('Content-Type: text/xml');
  echo $xml_feed;
  exit;
}

/**
 * Outputs product feed using a customizable template. Data collection is
 * done by _q_prodfeeds_prepare_product(), so this function itself is generic.
 */
function q_prodfeeds_generic($template_name, $feed_selector = '') {

  if (!$feed_selector) {
    $feed_selector = $template_name;
  }

  $xml_feed = '';
  while ($result = _q_prodfeeds_get_products($template_name, $feed_selector)) {

    foreach ($result as $entity) {
      if (!empty($entity->nid)) {

        $cdata = array();
        // Just trying to keep the output as close as possible to what it was
        // (I don't know if this is actually required):
        if ($template_name == 'affiliate4you') {
          // A4Y in this module has things in CDATA
          $cdata = array(
            'title' => TRUE,
            'description' => TRUE,
          );
        }
        $product = _q_prodfeeds_prepare_product($entity, $template_name,
          array(
            // Old GBW had the whole paragraph, HTML, in the description for A4Y feeds.
            // Affiliate4You - Aanleverspecificaties Adverteerders April 2011 says:
            //  "Hierin geen HTML code, returns of tabs"
            'field_product_desc' => 'text_plain',
          ),
          $cdata
        );


        $xml_feed .= theme('q_prodfeeds_item_' . $template_name,
          array('item' => $product));
      }
    }
  }

  // if ($xml_feed) { No; also output XML if feed contains no items.
  $xml_feed = theme('q_prodfeeds_feed_' . $template_name,
    array(
      'feed' => array(
        'data' => $xml_feed,
      )
    )
  );

  header('Content-Type: text/xml');
  echo $xml_feed;
  exit;
}

///

/**
 * Form builder, returning settings form
 *
 * Value form for discounts. (Not enough code yet to split out into an .admin.inc)
 */
function q_prodfeeds_settings_form() {
  $form = array();

  $form['note'] = array ( '#markup' => '<div class="messages">' . t('NOTE: The code which generates the XML holds various site specific assumptions. XML feeds are NOT GUARANTEED to output correct information; test them well!') . '</div>');

  // The most basic-essential choices first:
  // 1) determine type which should be queried. Must have the selection field.

  $entity_bundle = variable_get('q_prodfeeds_entity_bundle', 'commerce_product:');

  $options = array(
    'commerce_product:' => 'Product (all types)',
  );
  foreach (field_info_bundles('commerce_product') as $bundle => $info) {
    $options['commerce_product:' . $bundle] = 'Product type: ' . $info['label'];
  }
  $options['node:' . $bundle] = 'Node (all types)';
  foreach (field_info_bundles('node') as $bundle => $info) {
    $options['node:' . $bundle] = 'Node: ' . $info['label'];
  }
  $form['q_prodfeeds_entity_bundle'] = array(
    '#title' => 'Entity type to select for inclusion in feeds',
    '#description' => 'NOTE: when changing this, you must submit this form and only <em>after</em> submitting, you can properly select a field from the next item!',
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $entity_bundle,
  );

  // 2) The field which holds the selection of feeds

  $options = array('' => t('- Please choose -'));

  $eb = explode(':', $entity_bundle);
  if (empty($eb[1])) {
    // First layer will be the bundles. Get rid of it; merge all bundles.
    $info_i = drupal_array_merge_deep_array(field_info_instances($eb[0]));
  }
  else {
    $info_i = field_info_instances($eb[0], $eb[1]);
  }
  $info_fields = field_info_fields();

  // The 'selector' determining whether products are part of a product feed is a
  // field - can be term reference or list_text type.
  // And THAT can be part of the product or the display node.
  foreach ($info_i as $fieldname => $info) {
    if (isset($info_fields[$fieldname]['type']) &&
        ($info_fields[$fieldname]['type'] == 'taxonomy_term_reference' ||
         $info_fields[$fieldname]['type'] == 'list_text')
    ) {
      $options[$fieldname] = $info['label'];
    }
  }
  $options_field = variable_get('q_prodfeeds_apifield', '');
  $form['q_prodfeeds_apifield'] = array(
    '#title' => 'Field determining inclusion in feeds',
    '#description' => 'NOTE: when changing this, you must submit this form and only <em>after</em> submitting, you can properly select field values in the form below!',
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $options_field,
  );

  if ($options_field && isset($info_fields[$options_field])) {


    // Build term options; only for 'product' vocabularies.
    $options = array('' => t('- Please choose -'));

    if ($info_fields[$options_field]['type'] == 'taxonomy_term_reference') {

      if (isset($info_fields[$options_field]['settings']['allowed_values'])
          && is_array($info_fields[$options_field]['settings']['allowed_values'])) {

        $vocs = taxonomy_get_vocabularies();
        foreach ($info_fields[$options_field]['settings']['allowed_values'] as $info) {
          if (isset($info['vocabulary'])) {
            // Look up vid
            $vocab = NULL;
            foreach ($vocs as $voc) {
              if ($voc->machine_name == $info['vocabulary']) {
                $vocab = $voc;
                break;
              }
            }
            if (isset($vocab)) {
              // Get terms in 2-layer option array. You will very likely have
              // only one vocabulary (i.e. the outer foreach has only one
              // iteration) but you'll see the vocabulary name, at least.
              foreach (taxonomy_get_tree($vocab->vid) as $term) {
                $options[$vocab->name][$term->tid] = check_plain($term->name);
              }
            }
          }
        }
      }
    }
    elseif (isset($info_fields[$options_field]['settings']['allowed_values'])) {
      $options += $info_fields[$options_field]['settings']['allowed_values'];
    }

    // How to get all possible menu items? We can use hook_menu. Since the URL
    // might change, let's take the function name (page callback) as keys for the
    // settings we want to store.
    // This also means that different menu items with the same page callback will
    // only receive one set of 'config items' -- which is good.
    $menu_items = q_prodfeeds_menu();
    $feeds_info = array();
    foreach ($menu_items as $path => $item) {
      if ($item['access arguments'] == array('access content')
          && strpos($item['page callback'], 'q_prodfeeds_') === 0) {
        $key = str_replace('q_prodfeeds_', '', $item['page callback']);
        $feeds_info[$key] = $item['title'];
      }
    }

    foreach ($feeds_info as $key => $title) {
      $form[$key] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#title' => $title,
      );
      $form[$key]['q_prodfeeds_filter_' . $key] = array(
        '#type'          => 'select',
        '#title'         => t('Field value used to assign products to this feed'),
        '#options'       => $options,
        '#default_value' => variable_get('q_prodfeeds_filter_' . $key, ''),
      );
      $form[$key]['q_prodfeeds_limit_' . $key] = array(
        '#type' => 'textfield',
        '#title' => t('Maximum number of items returned (0 is unlimited)'),
        '#default_value' => variable_get('q_prodfeeds_limit_' . $key, '0'),
      );

      // Feed specific options
      switch ($key) {
        case 'affiliate4u':
          break;

        case 'google':
          $form[$key]['q_prodfeeds_myhost_' . $key] = array(
            '#type' => 'textfield',
            '#title' => t('Host name used in feed'),
            '#description' => t("This system's hostname. The feed will work without filling this, but if the URL can be reached by way of several different paths (e.g. with and without 'www.') it's better to fill this, to have canonical URLs"),
            '#default_value' => variable_get('q_prodfeeds_myhost_' . $key, ''),
          );

          // no break!

        case 'tradetracker':
          $form[$key]['q_prodfeeds_bodytype_' . $key] = array(
            '#type' => 'select',
            '#title' => t('Node text type'),
            '#description' => t('What do we want to output?'),
            '#options' => array('teaser' => 'Teaser', 'body' => 'Body'),
            '#default_value' => variable_get('q_prodfeeds_bodytype_' . $key, 'teaser'),
          );

          break;
      }

    }
  }

  return system_settings_form($form);
}
