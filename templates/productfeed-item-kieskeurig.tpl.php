<?php
/**
 * Item in Kieskeurig feed (kieskeurig.nl).
 *
 * Information about Kieskeurig feed:
 * - http://www.kieskeurig.nl/faq
 * - http://images.kieskeurig.nl/images/leaflets/Kieskeurig.nl_informatie_aanbiedingstekst.pdf,
 *   (linked from http://www.kieskeurig.nl/partners)
 *
 * Tag names are apparently not prescribed (as seen on http://www.kieskeurig.nl/document/4579E5AA0B694EEE828602A3A026EB12)
 *
 * http://www.kieskeurig.nl/document/F56F6B255B0D4650BF7E4B5397D6F15D indicates that
 *  ProductsInStock is unneccessary. They refer to one stock field, and treat 1 or >1 equally.
 *  That would indicate StockStatus & ProductsInStock are duplicates; this needs
 *  more info...
 * It also indicates very specific options for delivery time (not implemented
 * yet; this depends on website-specific values):
 *   LEVERTIJD
 *   [...]
 *   Gewenste indeling:
 * - Op werkdagen voor [xx.xx] betaald, morgen in huis
 * - 1 t/m 2 werkdagen
 * - 3 t/m 4 werkdagen
 * - t/m 5 werkdagen
 * - Langer dan 5 werkdagen
 * - onbekend
 *
 * <PromotionText>: http://images.kieskeurig.nl/images/leaflets/Kieskeurig.nl_informatie_aanbiedingstekst.pdf
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
$i = $variables;
?>
<Product>
  <Id><?php                 print $i['sku']         ?></Id>
  <Category><?php           print $i['category']    ?></Category>
  <SubCategory><?php        print $i['subcategory'] ?></SubCategory>
  <Brand><?php              print $i['brand'];      ?></Brand>
  <Type><?php               print $i['type'];       ?></Type>
  <ProductName><?php        print $i['title'];      ?></ProductName>
  <Deeplink><?php           print $i['url'];        ?></Deeplink>
  <Price><?php              print $i['price'];      ?></Price>
  <DeliveryPeriod><?php     print $i['delivery'];   ?></DeliveryPeriod>
  <DeliveryCosts><?php      print $i['shipping'];   ?></DeliveryCosts>
  <OfferId></OfferId>
  <ProductVendorPartNr></ProductVendorPartNr>
  <ProductEAN><?php         print $i['ean'];                  ?></ProductEAN>
  <ProductDescription><?php print $i['description'];          ?></ProductDescription>
  <DeeplinkPicture><?php    print $i['image_url'];            ?></DeeplinkPicture>
  <StockStatus><?php // 0 if 0, but empty if ''.
    print empty($i['stock']) ? $i['stock'] : 1; ?></StockStatus>
  <ProductsInStock><?php    print $i['stock'];                ?></ProductsInStock>
  <PromotionText></PromotionText>
</Product>
