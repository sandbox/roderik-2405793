<?php
/**
 * Item in Affiliate4You feed (Affiliate4You.nl).
 *
 * Tag names are apparently not prescribed; from their specs:
 * Naamgeving is variabel, id mag dus bijvoorbeeld ook 'product_id' heten of 'artikelnummer'. Wij matchen de veldnamen intern aan onze eigen data structuur.
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
$i = $variables;
?>
<product>
  <id><?php            print $i['sku'];         ?></id>
  <eancode><?php       print $i['ean'];         ?></eancode>
  <url><?php           print $i['url'];         ?></url>
  <afbeelding><?php    print $i['image_url'];   ?></afbeelding>
  <naam><?php          print $i['title'];       ?></naam>
  <omschrijving><?php  print $i['description']; ?></omschrijving>
  <merk><?php          print $i['brand'];       ?></merk>
  <type><?php          print $i['type'];        ?></type>
  <categorie><?php     print $i['category']     ?></categorie>
  <subcategorie><?php  print $i['subcategory']  ?></subcategorie>
  <prijs><?php         print $i['price'];       ?></prijs>
  <verzendkosten><?php print $i['shipping'];    ?></verzendkosten>
  <levertijd><?php     print $i['delivery'];    ?></levertijd>
  <voorraad><?php      print $i['stock'];       ?></voorraad>
</product>
