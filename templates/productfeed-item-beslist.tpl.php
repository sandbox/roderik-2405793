<?php
/**
 * Item in Beslist feed. (beslist.nl)
 *
 * This was taken over literally from a years-old custom module, untested.
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
$i = $variables;
?>
<Product>
  <Titel><?php             print $i['title'];       ?></Titel>
  <EAN><?php               print $i['ean'];         ?></EAN>
  <Merk><?php              print $i['brand'];       ?></Merk>
  <SKU><?php               print $i['sku'];         ?></SKU>
  <Beschrijving><?php      print $i['description']; ?></Beschrijving>
  <Prijs><?php             print $i['price'];       ?></Prijs>
  <Levertijd><?php         print $i['delivery'];    ?></Levertijd>
  <Deeplink><?php          print $i['url'];         ?></Deeplink>
  <Imagelocatie><?php      print $i['image_url'];   ?></Imagelocatie>
  <Categorie><?php         print $i['category'];    ?></Categorie>
  <Portokosten><?php       print $i['shipping'];    ?></Portokosten>
  <Winkelproductcode><?php print $i['local_id'];    ?></Winkelproductcode>
</Product>
