<?php
/**
 * Item in Vergelijk feed (vergelijk.nl).
 *
 * This was taken over literally from a years-old custom module, untested, and
 * may need some cleanup. (Category vs type? But then subcategory?)
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
$i = $variables;
?>
<product>
  <id><?php            print $i['local_id'];  ?></id>
  <productcode><?php   print $i['sku'];       ?></productcode>
  <merk><?php          print $i['brand'];     ?></merk>
  <type><?php          print $i['category'];  ?></type>
  <toevoeging-type></toevoeging-type>
  <extra-productbeschrijving><?php print $i['description']; ?></extra-productbeschrijving>
  <partnumber></partnumber>
  <ean-code><?php      print $i['ean'];       ?></ean-code>
  <prijs><?php         print $i['price'];     ?></prijs>
  <verzendkosten><?php print $i['shipping'];  ?></verzendkosten>
  <afhaalkosten>0.00</afhaalkosten>
  <levertijd><?php     print $i['delivery'];  ?></levertijd>
  <deeplink><?php      print $i['url'];       ?></deeplink>
  <imagelink><?php     print $i['image_url']; ?></imagelink>
  <voorraad><?php      print $i['stock'];     ?></voorraad>
</product>
