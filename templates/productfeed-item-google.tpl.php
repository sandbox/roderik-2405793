<?php
/**
 * Item in Google Merchant feed.
 *
 * Google unique product identifiers:
 * http://support.google.com/merchants/bin/answer.py?hl=en&answer=160161
 * MPN = Manufacturer Part Number = Global  -> in g:mpn
 * EAN = Manufacturer Part Number = Global  -> in g:gtin
 * At least brand + one of these 2 are required.
 * (Used to be that mpn was always required, I think.)
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
$i = $variables;
?>
<entry>
  <id><?php             print $i['sku'];         ?></id>
  <link><?php           print $i['url'];         ?></link>
  <title><?php          print $i['title'];       ?></title>
  <description><?php    print $i['description']; ?></description>
  <g:price><?php        print $i['price'];       ?></g:price>
  <g:condition>new</g:condition>
  <g:gtin><?php         print $i['ean'];         ?></g:gtin>
  <g:image_link><?php   print $i['image_url'];   ?></g:image_link>
  <g:brand><?php        print $i['brand'];       ?></g:brand>
  <g:product_type><?php print $i['category'];    ?></g:product_type>
  <g:availability><?php print
      // If stock is not filled, we print 'in stock'.
      ($i['stock'] > 0 || $i['stock'] === '') ? 'in stock' : 'out of stock'; ?></g:availability>
  <g:shipping>
    <g:country><?php    print $i['shipping_country']; ?></g:country>
    <g:price><?php      print $i['shipping'];    ?></g:price>
  </g:shipping>
  <g:featured_product>n</g:featured_product>
</entry>
