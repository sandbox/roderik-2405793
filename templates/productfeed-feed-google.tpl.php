<?php
/**
 * Google Merchant feed, Atom 1.0 format.
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
?>
<feed xmlns="http://www.w3.org/2005/Atom" xmlns:g="http://base.google.com/ns/1.0">
  <title><?php print $variables['title'] ?> products</title>
  <author><name><?php print $variables['author'] ?></name></author>
  <link rel="self" type="application/atom+xml" href="<?php print $variables['full_url'] ?>"/>
  <id>tag:<?php print $variables['hostname'] . ',' . str_replace(':', '', $variables['last_update']) . ':/' . $variables['local_url'] ?></id>
  <updated><?php print $variables['last_update'] ?></updated>
  <?php print $variables['payload']?>
</feed>
