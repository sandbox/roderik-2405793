<?php
/**
 * Item in Tradetracker feed.
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
$i = $variables;
?>
<product id="<?php print $i['local_id']; ?>">
  <name><?php        print $i['title'];       ?></name>
  <price><?php       print $i['price'];       ?></price>
  <description><?php print $i['description']; ?></description>
  <productURL><?php  print $i['url'];         ?></productURL>
  <imageURL><?php    print $i['image_url'];   ?></imageURL>
  <categories>
    <category name="<?php print $i['category'] ?>" />
  </categories>
  <additional>
    <field name="EAN" value="<?php print $i['ean'] ?>" />
    <field name="SKU" value="<?php print $i['sku'] ?>" />
  </additional>
</product>
