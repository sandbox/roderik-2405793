<?php
/**
 * Tradetracker feed.
 *
 * Note this template is not part of Drupal's theme system! It is defined in a
 * hook_default_productfeeds(_alter) implementation.
 */
// Print first header line using PHP instead of placing it literally in this
// file before the '<?php', to prevent confusion for old PHP versions
// interpreting any '<?' as PHP.
print '<?xml version="1.0" encoding="' . $variables['encoding'] . '"?>'; ?>
<productFeed version="1.0" timestamp="<?php print $variables['last_update'] ?>">
<?php print $variables['payload']?>
</productFeed>
