If you find an archive file inside this directory, it is probably the version
1.x of this module. This has NOT been put under version control since it has
settings in code. It can be removed when you're SURE you never need to
look at the previous code any more, to trace any possible changes in the
feeds.

Version 2 of this module has some 'feature creep': a default view, which I
needed to install on all the sites where I also installed the feeds - because
it makes adding all the feed-related tags to products easier.
This introduces a dependency on views_bulk_operations.

I don't much like the 'editview' type view myself, which is why it doesn't
have a menu item by default. Johan liked it at first, so I'm going to install
it everywhere until we figure out whether we want it.
This means the module does have a dependency on the 'editview' module for now.

NOTE: The view has fields/filters with vocabulary-related settings.
Since these vocabularies are suposedly present in the site already, and have
no standard name or vid, it is likely you'll need to change these settings.
* 'product categories' - in the view's edit screen: exposed filter "Categories".
* 'produkt speciaal' -- containing the terms related to 'inclusion in various
  feeds'. In the view's edit screen: field "All terms"

--Roderik Muit
